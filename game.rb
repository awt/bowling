require 'pp'

class Game
  STRIKE = 10
  LAST_FRAME_INDEX = 9
  MAX_FRAMES = 10
  def initialize
    @current_rolls = []
    @frames = []
  end

  def current_frame
    frame = nil
    if ( @frames[-1] )
      if( @frames[-1].size < balls_per_frame)
        frame = @frames.pop
      else
        frame = []
      end
    else
      frame = []
    end
  end

  #return 3 balls if there are two or less balls rolled and they add up to 10
  def balls_per_frame
    if @frames.size == 10 and (frame_pin_count(@frames[-1]) >= STRIKE)
      return 3
    else
      return 2
    end
  end

  def frame_pin_count(frame)
    (frame[0].nil? ? 0 : frame[0]) + (frame[1].nil? ? 0 : frame[1])
  end

  def roll(pin_count)
    raise "invalid pin count" if pin_count > 10
    frame = current_frame
    #what if this is the last frame?
    frame.push pin_count
    if(pin_count == STRIKE && ((@frames.size + 1) != MAX_FRAMES))
      frame.push 0
    end
    @frames.push(frame)
  end

  def score
    return score_frames
  end

  def spare?(frame)
    return false if frame[0] == STRIKE
    return (frame[0] + frame[1] == STRIKE)
  end

  def strike?(frame)
    return (frame[0] == STRIKE)
  end

  def sum_of_next_two_balls(frame_index)
    remaining_balls = @frames[frame_index + 1 .. -1].collect do |f|
      if f.size == 2
        if f[0] == STRIKE
          [STRIKE]
        else
          f
        end
      elsif f.size == 3
        f
      end
    end.flatten

    if remaining_balls.length >= 2
      return remaining_balls[0] + remaining_balls[1]
    elsif remaining_balls.lenth == 1
      return remaining_balls[1]
    else
      return 0
    end
    
  end

  def score_frames
    #puts "\n\n\n\n"
    #pp @frames
    sum = 0
    @frames.each_with_index do |frame, i|
      next_frame = @frames[i+1]
      frame_after_next = @frames[i+2]

      sum += frame_pin_count(frame)
      if next_frame and spare?(frame)
        sum += next_frame[0]
      elsif next_frame and strike?(frame)
        sum += sum_of_next_two_balls(i)
      elsif i == LAST_FRAME_INDEX and (frame_pin_count(frame) >= STRIKE)
        sum += frame[2] if !frame[2].nil?
      end
      puts "frame #{i + 1}: #{sum}"
    end
    puts "score: #{sum}"
    return sum
  end
end

