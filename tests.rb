require_relative 'game'

game = Game.new

#one roll
game.roll(1)
if (game.score != 1)
  puts "test 1 failed"
else
  puts "test 1 succeeded"
end

#three rolls no spare no strike

game = Game.new
game.roll(1)
game.roll(1)
game.roll(1)

if (3 != game.score)
  puts "test  2 failed"
else
  puts "test 2 succeeded"
end

#three rolls with spare

game = Game.new
game.roll(1)
game.roll(9)
game.roll(1)
game.roll(1)


if( 13 != game.score )
  puts "test 3 failed"
else
  puts "test 3 succeeded"
end

#three rolls with strike
game = Game.new
game.roll(10)
game.roll(1)
game.roll(1)
game.roll(1)
game.roll(1)

if(16 != game.score)
  puts "test 4 failed"
else
  puts "test 4 succeeded"
end


#test 10th frame special rules
game = Game.new
rolls = [1,4,4,5,6,4,5,5,10,0,1,7,3,6,4,10,2,8,6]
rolls.each do |r|
  game.roll(r)
end

score = game.score
if(133 != score)
  puts "test 5 failed: #{score}" 
else
  puts "test 5 succeeded"
end

#test 2 strikes on 10th frame
game = Game.new
rolls = [10,8,2,10,6,2,10,10,10,5,5,8,2,10,10,9]
rolls.each do |r|
  game.roll(r)
end

score = game.score
if(208 != score)
  puts "test 6 failed: #{score}" 
else
  puts "test 6 succeeded"
end

#test 12 strikes
game = Game.new
rolls = [10,10,10,10,10,10,10,10,10,10,10,10]
rolls.each do |r|
  game.roll(r)
end

score = game.score
if(300 != score)
  puts "test 7 failed: #{score}" 
else
  puts "test 7 succeeded"
end

#test 10 strikes
game = Game.new
rolls = [10,10,10,10,10,10,10,10,10,10,0,0]
rolls.each do |r|
  game.roll(r)
end

score = game.score
if(270 != score)
  puts "test 8 failed: #{score}" 
else
  puts "test 8 succeeded"
end

#test 11 strikes
game = Game.new
rolls = [10,10,10,10,10,10,10,10,10,10,10,0]
rolls.each do |r|
  game.roll(r)
end

score = game.score
if(290 != score)
  puts "test 9 failed: #{score}" 
else
  puts "test 9 succeeded"
end


#This test should cause an exception since the pin count is too high
game = Game.new
rolls = [11,10,10,10,10,10,10,10,10,10,10,0]
rolls.each do |r|
  begin
    game.roll(r)
  rescue
    puts "test 10 succeeded"
  end
end
